﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
[RequireComponent (typeof(AudioSource))]
public class PlaneMovement : MonoBehaviour {
	//main plane = gameObject this sscript is assigned to
	GameObject mainPlane;
	//broken plane disabled object
	public GameObject brokenPlane;
	//Senstivity of the plane
	public float liftSpeed = 2.0f;
	//What the tilt angle of the plane should be
	public float tiltAngle = 45.0f;
	//Fly up or down?
	float clickControl = 0.0f;
	//Game started?
	public bool started = false;
	//Are we dead?
	public bool isDead = false;
	//Lock value for x axis
	float lockX = 0.0f;
	//lefped click control value to have a bit more smooth effect
	float currentClickHoldValue = 0.0f;

    public float speed = 6;
	//audio source attached to the game object
	AudioSource myAudio;
	// Use this for initialization
	void Start () {
        //Setting variables and default value

        GetComponent <Rigidbody2D> ().velocity = Vector2 .right * speed;

        Invoke("StartIn", 0.5f);

        mainPlane = gameObject;
		mainPlane.SetActive (true);
	//	brokenPlane.SetActive (false);
		lockX = -5.15f;
		myAudio = transform.GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		//if we are dead then don't do anything
		if (isDead)
			return;
		//Up and down movement input
		if (Input.GetKey (KeyCode.Mouse0) && started) {
			clickControl = 1.0f;
		} else if (started){
			clickControl = -1.0f;
		}
		//Set the position of the plane
		// mainPlane.transform.position = new Vector3 (lockX, mainPlane.transform.position.y, mainPlane.transform.position.z);
		//if the game haven't been started the fly on your own
		if (!started) {
		//	currentClickHoldValue = Mathf.PingPong (Time.time, 2.0f) - 1.0f;
			mainPlane.transform.Translate (Vector3.up * (currentClickHoldValue * Time.deltaTime * 1.5f));
		} else {
		//	lockX = Mathf.Lerp(lockX,-2.15f,Time.deltaTime*1.0f);
			currentClickHoldValue = Mathf.Lerp (currentClickHoldValue, clickControl, Time.deltaTime * 3.0f);
			mainPlane.transform.Translate (Vector3.up * (currentClickHoldValue * Time.deltaTime * liftSpeed));
		}
		//change pitch according to input
		myAudio.pitch = 2.0f + currentClickHoldValue;
		//Tilt the plane
		mainPlane.transform.rotation = Quaternion.Euler(0.0f,0.0f,Mathf.LerpAngle(mainPlane.transform.eulerAngles.z,currentClickHoldValue*tiltAngle,Time.deltaTime*5.0f));
		

	}
	//Detect collision
	void OnTriggerEnter2D (Collider2D col){
        if (col.name == "collide")
        {
            Application.LoadLevel(Application.loadedLevel);
        }

        if (col.name == "win")
        {
            SceneManager.LoadScene("Gattaca Title Screen");
        }

    }
	//Die
	void Die (){
	//	isDead = true;
	//	brokenPlane.transform.position = mainPlane.transform.position;
	//	brokenPlane.transform.eulerAngles = mainPlane.transform.eulerAngles;
	//	mainPlane.SetActive(false);
	//	brokenPlane.SetActive (true);
	}
	//Start the game in 0.5 seconds
	void StartTheGame (){
		Invoke ("StartIn", 0.5f);
	}
	//start the game
	void StartIn (){
		started = true;
	}
}
