﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
using UnityEngine.SceneManagement;

public class LevelChecker : MonoBehaviour
{
    public Fungus.Flowchart myFlowChart;

    SpriteRenderer fadeComponent;
    Color temptColor;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log(temptColor = GameObject.Find("Fade").GetComponent<SpriteRenderer>().color);
        temptColor = GameObject.Find("Fade").GetComponent<SpriteRenderer>().color;
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(myFlowChart.GetBooleanVariable("Correct1"));
        Debug.Log(myFlowChart.GetBooleanVariable("Correct2"));

        if (myFlowChart.GetBooleanVariable("Correct2") && (myFlowChart.GetBooleanVariable("Correct1")))
        {
            temptColor.a += 0.1f;
            GameObject.Find("Fade").GetComponent<SpriteRenderer>().color = temptColor;
            Debug.Log(temptColor = GameObject.Find("Fade").GetComponent<SpriteRenderer>().color);

            if (temptColor.a >= 3)
            {
                SceneManager.LoadScene("Wang-Puzzle5");
            }
        }
    }
}
