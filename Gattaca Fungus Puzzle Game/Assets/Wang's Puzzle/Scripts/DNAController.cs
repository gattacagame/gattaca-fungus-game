﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DNAController : MonoBehaviour {
    public GameObject _dna;

    public int dnaID;

    private Transform insDna;

    public RectTransform[] dnaRect;

    public void BeginDrag()
    {
        insDna = Instantiate(_dna, transform.parent).transform;
    }

    public void EndDrag()
    {
        for(int i =0;i< dnaRect.Length;i++)
        {
            if (dnaRect[i].GetComponent<DNARect>().isIn)
            {
                if (dnaRect[i].GetComponent<DNARect>().SetDna(insDna, dnaID))
                    return;
            }
        }
        Destroy(insDna.gameObject);
    }

    public void Drag()
    {
        insDna.position = Input.mousePosition;
    }
}
