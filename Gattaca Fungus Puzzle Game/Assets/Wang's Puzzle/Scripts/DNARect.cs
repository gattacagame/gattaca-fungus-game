﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DNARect : MonoBehaviour {

    public int rightid;

    public bool isright;

    private Transform _dna;

    private Transform draged;

    public RectTransform[] dnaRect;
    public bool isIn = false;

    private int dnaID;

    public bool SetDna(Transform _dna , int num)
    {
        if (this._dna != null)
            return false;
        this._dna = _dna;
        dnaID = num;
        if (rightid == num)
            isright = true;
        else
            isright = false;

        _dna.position = transform.position;
    //    _dna.GetComponent<CanvasGroup>().blocksRaycasts = true;
        return true;
    }

    public void MouseEnter()
    {
        isIn = true;
    }

    public void MouseExit()
    {
        isIn = false;
    }

    public void BeginDrag()
    {
        draged = _dna;
        _dna = null;
        isright = false;
    }

    public void EndDrag()
    {
        for (int i = 0; i < dnaRect.Length; i++)
        {
            if (dnaRect[i].GetComponent<DNARect>().isIn)
            {
                if (dnaRect[i].GetComponent<DNARect>().SetDna(draged, dnaID))
                    return;
            }
        }
        Destroy(draged.gameObject);
    }

    public void Drag()
    {
        draged.position = Input.mousePosition;
    }
}
