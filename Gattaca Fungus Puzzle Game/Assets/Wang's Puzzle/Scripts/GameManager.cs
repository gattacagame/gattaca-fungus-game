﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public DNARect[] allrect;

    public Image rightLight;

    public Sprite rightSprite;

    public GameObject endui;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		for(int i = 0; i<allrect.Length;i++)
        {
            if (!allrect[i].isright)
                return;
        }
        rightLight.sprite = rightSprite;
        endui.SetActive(true);
    }

    public void Again()
    {
        SceneManager.LoadScene("Taylor-Puzzle3");
    }

    public void Quit()
    {
        Application.Quit();
    }
}
